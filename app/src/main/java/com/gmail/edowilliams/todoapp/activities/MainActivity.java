package com.gmail.edowilliams.todoapp.activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.gmail.edowilliams.todoapp.R;
import com.gmail.edowilliams.todoapp.adapters.TodoItemAdapter;
import com.gmail.edowilliams.todoapp.fragments.EditItemFragment;
import com.gmail.edowilliams.todoapp.models.TodoItem;
import com.gmail.edowilliams.todoapp.utils.ItemClickSupport;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;

public class MainActivity extends AppCompatActivity implements EditItemFragment.EditItemFragmentListener, TodoItemAdapter.OnClickListener {
    private static final String TAG = MainActivity.class.getSimpleName();
    private List<TodoItem> mTodoItems;
    private RecyclerView.Adapter mTodoItemsAdapter;
    private RecyclerView mRecyclerView;
    private FirebaseFirestore db;
    private FirebaseUser user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        db = FirebaseFirestore.getInstance();

        if (getIntent().getExtras() != null) {
            user = (FirebaseUser) getIntent().getExtras().get("user");
        }

        mTodoItems = TodoItem.getAll();

        mRecyclerView = (RecyclerView) findViewById(R.id.listViewItems);
        mRecyclerView.setItemAnimator(new SlideInUpAnimator());
        mTodoItemsAdapter = new TodoItemAdapter(this, mTodoItems, this);
        mRecyclerView.setAdapter(mTodoItemsAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        setupListViewListener();
    }

    public void onAddItem(View view) {
        EditText editTextNewItem = (EditText) findViewById(R.id.editTextNewItem);
        String itemText = editTextNewItem.getText().toString();

        if (!itemText.isEmpty()) {
            TodoItem todoItem = new TodoItem(itemText);
            todoItem.save();
            mTodoItems.add(todoItem);
            int newPosition = mTodoItems.size() - 1;
            mTodoItemsAdapter.notifyItemInserted(newPosition);
            mRecyclerView.scrollToPosition(mTodoItemsAdapter.getItemCount() - 1);
            editTextNewItem.setText("");
            saveOrUpdateToCloud(todoItem);
        }
    }

    private void setupListViewListener() {

        ItemClickSupport.addTo(mRecyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                FragmentManager fragmentManager = getSupportFragmentManager();
                TodoItem todoItem = getItem(position);
                EditItemFragment editItemFragment = EditItemFragment.newInstance(todoItem.getTitle(), position);
                editItemFragment.show(fragmentManager, "fragment_edit_item");
            }
        });

//        ItemClickSupport.addTo(mRecyclerView).setOnItemLongClickListener(new ItemClickSupport.OnItemLongClickListener() {
//            @Override
//            public boolean onItemLongClicked(RecyclerView recyclerView, int position, View v) {
//                TodoItem todoItem = getItem(position);
//
//                todoItem.delete();
//                mTodoItems.remove(position);
//
//                mTodoItemsAdapter.notifyItemRemoved(position);
//                return true;
//            }
//        });

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int position = viewHolder.getAdapterPosition();
                TodoItem todoItem = getItem(position);

                todoItem.delete();
                mTodoItems.remove(position);

                mTodoItemsAdapter.notifyItemRemoved(position);
                deleteFromCloud(todoItem);
            }
        });
        itemTouchHelper.attachToRecyclerView(mRecyclerView);
    }

    @Override
    public void onFinishEditDialog(String newItemText, int itemPosition) {
        saveEdit(itemPosition, newItemText);
        Toast.makeText(this, "Edit saved", Toast.LENGTH_SHORT).show();
    }

    private TodoItem getItem(int position) {
        return mTodoItems.get(position);
    }

    private void saveEdit(int itemPosition, String newTitle) {
        TodoItem todoItem = getItem(itemPosition);
        todoItem.setTitle(newTitle);
        mTodoItemsAdapter.notifyItemChanged(itemPosition);
        saveOrUpdateToCloud(todoItem);
    }

    @Override
    public void onDeleteClick(int position) {
        TodoItem todoItem = getItem(position);
        todoItem.delete();
        mTodoItems.remove(position);
        mTodoItemsAdapter.notifyItemRemoved(position);
        deleteFromCloud(todoItem);
    }

    private void saveOrUpdateToCloud(TodoItem todoItem) {
        Map<String, Object> item = new HashMap<>();
        item.put("title", todoItem.getTitle());

        db.collection(user.getEmail())
                .document(todoItem.getId().toString())
                .set(item)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(getBaseContext(), "Saved successfully", Toast.LENGTH_LONG).show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error adding document", e);
                        Toast.makeText(getBaseContext(), "Error saving item", Toast.LENGTH_LONG).show();
                    }
                });
    }

    private void readFromCloud() {
        db.collection(user.getEmail())
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d(TAG, document.getId() + " => " + document.getData());
                            }
                        } else {
                            Log.w(TAG, "Error getting documents.", task.getException());
                        }
                    }
                });
    }

    private void deleteFromCloud(TodoItem todoItem){
        db.collection(user.getEmail())
                .document(todoItem.getId().toString())
                .delete()
                .addOnSuccessListener(this, new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.w(TAG, "Delete successful.");
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error deleting.");
                    }
                });
    }

    private void logout() {
        FirebaseAuth.getInstance().signOut();
    }
}