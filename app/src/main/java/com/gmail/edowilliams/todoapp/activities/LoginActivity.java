package com.gmail.edowilliams.todoapp.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.motion.MotionLayout;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.gmail.edowilliams.todoapp.R;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;
import java.util.List;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = LoginActivity.class.getSimpleName();
    private AutoCompleteTextView mEmail;
    private TextInputLayout mEmailLayout;
    private TextInputEditText mPassword;
    private TextInputLayout mPasswordLayout;
    private MotionLayout mMotionLayout;
    private List<String> emails;
    private FirebaseAuth auth;
    private Button mLogin, mRegister;
    private String email, password;
    private ProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        progress = new ProgressDialog(this);
        progress.setTitle("Please wait...");
        progress.setCancelable(false);
        progress.setCanceledOnTouchOutside(false);
        progress.setIndeterminate(true);

        auth = FirebaseAuth.getInstance();
        mEmail = findViewById(R.id.email);
        mEmailLayout = findViewById(R.id.email_layout);
        mPassword = findViewById(R.id.password);
        mPasswordLayout = findViewById(R.id.password_layout);
        mLogin = findViewById(R.id.login_button);
        mRegister = findViewById(R.id.register_button);
        mMotionLayout = findViewById(R.id.login_layout);

        mMotionLayout.transitionToEnd();
        mLogin.setOnClickListener(this);
        mRegister.setOnClickListener(this);

        emails = new ArrayList<>();
        if(emails != null) {
            ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, emails);
            mEmail.setAdapter(adapter);
            mEmail.setThreshold(1);
        }

        mPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                    login();
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser user = auth.getCurrentUser();
        if(user != null){
            moveOnToNextActivity(user);
        }
    }

    private boolean isValid(){
        email = mEmail.getText().toString().trim();
        if(TextUtils.isEmpty(email) || !Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            mEmailLayout.setError("Invalid email");
            mEmail.requestFocus();
            return false;
        }else {
            mEmailLayout.setError(null);
        }

        password = mPassword.getText().toString().trim();
        if(TextUtils.isEmpty(password) || password.length() < 8){
            mPasswordLayout.setError("Password can not be less than 8 digits");
            mPassword.requestFocus();
            return false;
        }else {
            mPasswordLayout.setError(null);
        }

        return true;
    }

    private void login(){
        if (!isValid()) return;

        if(!isNetworkAvailable(this)){
            Toast.makeText(this, "No internet connection", Toast.LENGTH_LONG).show();
            return;
        }

        progress.show();

        auth.signInWithEmailAndPassword(email, password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                progress.dismiss();
                if(task.isSuccessful()){
                    FirebaseUser user = auth.getCurrentUser();
                    if(user != null){
                        moveOnToNextActivity(user);
                    }
                } else {
                    Toast.makeText(getBaseContext(), "SomeThing is not right", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void register(){
        if (!isValid()) return;

        if(!isNetworkAvailable(this)){
            Toast.makeText(this, "No internet connection", Toast.LENGTH_LONG).show();
            return;
        }

        progress.show();

        auth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                progress.dismiss();
                if(task.isSuccessful()){
                    Toast.makeText(getBaseContext(), "Successful: An account was created for you.", Toast.LENGTH_LONG).show();
                }else {
                    Toast.makeText(getBaseContext(), "Something went wrong...", Toast.LENGTH_LONG).show();
                }
            }
        }).addOnFailureListener(this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                progress.dismiss();
                if(e instanceof FirebaseAuthUserCollisionException){
                  Toast.makeText(getBaseContext(), "This email has been used", Toast.LENGTH_LONG).show();
                }else if(e instanceof FirebaseAuthWeakPasswordException){
                    Toast.makeText(getBaseContext(), "Password provided is too weak, try a stronger password", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void moveOnToNextActivity(FirebaseUser user){
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("user", user);
        startActivity(intent);
        finish();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.register_button:
                register();
                break;
            case R.id.login_button:
                login();
                break;
        }
    }

    /**
     *
     * @param context
     * @return boolean
     */
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
